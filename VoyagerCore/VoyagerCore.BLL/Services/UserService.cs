﻿using System;
using System.Collections.Generic;
using System.Text;
using VoyagerCore.BLL.DTO;
using VoyagerCore.BLL.IServices;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using JWTSample.Helpers;
using VoyagerCore.DAL.Repositories;
using VoyagerCore.DAL.Entities;
using AutoMapper;
using VoyagerCore.DAL.UnitOfWork;

namespace VoyagerCore.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly AppSettings _appSettings;
        private readonly IUserRepository userRepository;
        public UserService(IOptions<AppSettings> appSettings, IUserRepository userRepository, IMapper mapper, IUnitOfWork unitOfWork)
        {
            _appSettings = appSettings.Value;
            this.userRepository = userRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public UserDTO Authenticate(string kullaniciAdi, string sifre)
        {
            var user = FindAccount(kullaniciAdi, sifre);

            // Kullanici bulunamadıysa null döner.
            if (user == null)
                return null;

            // Authentication(Yetkilendirme) başarılı ise JWT token üretilir.
            var tokenHandler = new JwtSecurityTokenHandler();
            //İmza için gerekli gizli anahtarımı alıyorum.
            var key = Encoding.ASCII.GetBytes(_appSettings.SecretKey);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                //Özel olarak şu Claimler olsun dersek buraya ekleyebiliriz.
                Subject = new ClaimsIdentity(new[]
                {
                    //İstersek string bir property istersek ClaimsTypes sınıfının sabitlerinden çağırabiliriz.
                    new Claim("userId", user.Id.ToString()),
                    new Claim(ClaimTypes.Name, user.Username)
                }),
                //Tokenın hangi tarihe kadar geçerli olacağını ayarlıyoruz.
                Expires = DateTime.UtcNow.AddMinutes(2),
                //Son olarak imza için gerekli algoritma ve gizli anahtar bilgisini belirliyoruz.
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            //Token oluşturuyoruz.
            var token = tokenHandler.CreateToken(tokenDescriptor);
            //Oluşturduğumuz tokenı string olarak bir değişkene atıyoruz.
            string generatedToken = tokenHandler.WriteToken(token);

            //Sonuçlarımızı tuple olarak dönüyoruz.
            // Sifre null olarak gonderilir.
            user.Password = null;

            return user;
        }

        public IEnumerable<UserDTO> GetAll()
        {
            var allUsers = userRepository.GetAll();
            List<UserDTO> userDtos = new List<UserDTO>();
            foreach (var item in allUsers)
            {
                var user = allUsers.SingleOrDefault(u => u.Id == item.Id);
                userDtos.Add(new UserDTO()
                {
                    Id = item.Id,
                    Username = item.Username,
                });
            }
            return userDtos;
            //Kullanicilar sifre olmadan dondurulur
        }
        public void SingUp(UserDTO userModel)
        {
            var user = new User()
            {
                Id = userModel.Id,
                FirstName = userModel.FirstName,
                Password = userModel.Password,
                Username = userModel.Username
            };
            userRepository.Add(user);
            Save();
        }

        private UserDTO FindAccount(string kullaniciAdi, string sifre)
        {
            List<User> accounts = userRepository.GetAll();
            var account = accounts.SingleOrDefault(x => x.FirstName == kullaniciAdi && x.Password == sifre);
            UserDTO accountDto = _mapper.Map<UserDTO>(account);
            return accountDto;
        }
        public void Save()
        {
            _unitOfWork.Save();
        }
    }
}
