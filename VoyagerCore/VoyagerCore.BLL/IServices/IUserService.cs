﻿using System;
using System.Collections.Generic;
using System.Text;
using VoyagerCore.BLL.DTO;

namespace VoyagerCore.BLL.IServices
{
    public interface IUserService
    {
        UserDTO Authenticate(string kullaniciAdi, string sifre);
        IEnumerable<UserDTO> GetAll();
    }
}
