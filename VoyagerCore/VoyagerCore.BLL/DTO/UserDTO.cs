﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoyagerCore.BLL.DTO
{
    public  class UserDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; } = "unnamed";
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
