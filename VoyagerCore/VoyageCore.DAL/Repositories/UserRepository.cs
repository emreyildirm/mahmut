﻿using System;
using System.Collections.Generic;
using System.Text;
using VoyagerCore.DAL.Entities;

namespace VoyagerCore.DAL.Repositories
{
    public class UserRepository :  BaseRepository<User>, IUserRepository
    {
        public UserRepository(VoyagerContext Context) : base(Context)
        {

        }
    }
}
