using AutoMapper;
using JWTSample.Helpers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using VoyageCore.DAL.Repositories;
using VoyagerCore.BLL;
using VoyagerCore.BLL.IServices;
using VoyagerCore.BLL.Services;
using VoyagerCore.DAL;
using VoyagerCore.DAL.Repositories;
using VoyagerCore.DAL.UnitOfWork;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Text;

namespace VoyagerCore.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ////Ataca��m�z isteklerde CORS problemi ya�amamak i�in:
            //services.AddCors();
            //services.AddControllers();

            //// appsettings.json i�inde olu�turdu�umuz gizli anahtar�m�z� AppSettings ile �a��raca��m�z� s�yl�yoruz.
            //var appSettingsSection = Configuration.GetSection("AppSettings");
            //services.Configure<AppSettings>(appSettingsSection);
            
            //services.Configure<AppSettings>(appSettingsSection);
            //// Olu�turdu�umuz gizli anahtar�m�z� byte dizisi olarak al�yoruz.
            //var appSettings = appSettingsSection.Get<AppSettings>();
            //var key = Encoding.ASCII.GetBytes(appSettings.SecretKey);

            ////Projede farkl� authentication tipleri olabilece�i i�in varsay�lan olarak JWT ile kontrol edece�imizin bilgisini kaydediyoruz.
            //services.AddAuthentication(x =>
            //{
            //    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //})
            //    //JWT kullanaca��m ve ayarlar� da �unlar olsun dedi�imiz yer ise buras�d�r.
            //    .AddJwtBearer(x =>
            //    {
            //        //Gelen isteklerin sadece HTTPS yani SSL sertifikas� olanlar� kabul etmesi(varsay�lan true)
            //        x.RequireHttpsMetadata = false;
            //        //E�er token onaylanm�� ise sunucu taraf�nda kay�t edilir.
            //        x.SaveToken = true;
            //        //Token i�inde neleri kontrol edece�imizin ayarlar�.
            //        x.TokenValidationParameters = new TokenValidationParameters
            //        {
            //            //Token 3.k�s�m(imza) kontrol�
            //            ValidateIssuerSigningKey = true,
            //            //Neyle kontrol etmesi gerektigi
            //            IssuerSigningKey = new SymmetricSecurityKey(key),
            //            //Bu iki ayar ise "aud" ve "iss" claimlerini kontrol edelim mi diye soruyor
            //            ValidateIssuer = false,
            //            ValidateAudience = false
            //        };
            //    });

            

            //DI
            services.AddScoped<IBusService, BusService>();
            services.AddScoped<IDriverService, DriverService>();
            services.AddScoped<IExpeditionService, ExpeditionService>();
            services.AddScoped<IHostService, HostService>();
            services.AddScoped<IPassengerService, PassengerService>();
            services.AddScoped<IRouteService, RouteService>();
            services.AddScoped<ITicketService, TicketService>();
            services.AddScoped<IUserService, UserService>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IBusRepository, BusRepository>();
            services.AddScoped<IDriverRepository, DriverRepository>();
            services.AddScoped<IExpeditionRepository, ExpeditionRepository>();
            services.AddScoped<IHostRepository, HostRepository>();
            services.AddScoped<IPassengerRepository, PassengerRepository>();
            services.AddScoped<IRouteRepository, RouteRepository>();
            services.AddScoped<ITicketRepository, TicketRepository>();
            services.AddScoped<ISoldTicketRepository, SoldTicketRepository>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<VoyagerContext, VoyagerContext>();
            #region DbContext
            services.AddDbContext<VoyagerContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            #endregion
            services.AddMvc();

            services.AddControllersWithViews();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllerRoute(
            //        name: "default",
            //        pattern: "{controller}/{action=Index}/{id?}");
            //});

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
